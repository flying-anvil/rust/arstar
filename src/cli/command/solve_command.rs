use std::error::Error;

use clap::Args;

#[derive(Args, Clone, Debug)]
pub struct SolveCommand {
    /// Specify a grid to use. Omit to generate a random one.
    #[arg(short='g', long="grid")]
    grid_image: Option<String>,
}

impl SolveCommand {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        Err("Not yet implemented".into())
    }
}
