use std::error::Error;

use clap::Subcommand;

mod debug_grid_command;
pub use debug_grid_command::*;

#[derive(Subcommand, Clone, Debug)]
pub enum DebugCommand {
    #[command(name = "grid")]
    /// Display the grid
    Grid(DebugGridCommand),
}

impl DebugCommand {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        match self {
            DebugCommand::Grid(command) => command.run(),
        }
    }
}
