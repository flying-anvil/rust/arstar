use core::panic;
use std::{
    error::Error,
    io::{stdout, Write}, path::Path,
};

use clap::Args;
use image::{GenericImageView, Pixel};

#[derive(Args, Clone, Debug)]
pub struct DebugGridCommand {
    /// Specify an image to create a grid from. Omit to generate a grid.
    #[arg(short = 'i', long = "image")]
    grid_image: Option<String>,

    /// Print the grid using small blocks.
    #[arg(long = "small")]
    small: bool,
}

impl DebugGridCommand {
    pub fn run(&self) -> Result<(), Box<dyn Error>> {
        // println!("▀");
        // println!("██");

        let grid = match &self.grid_image {
            None => grid_dummy(30, 20),
            Some(image) => grid_from_image(image),
        };

        match self.small {
            true => display_grid_cli(&grid),
            false => display_grid_cli_big(&grid),
        }

        Ok(())
    }
}

fn grid_from_image(path: impl AsRef<Path>) -> Grid {
    let path = path.as_ref();
    if !path.exists() {
        panic!("Cannot load grid from image \"{}\" (does not exist)", path.to_str().unwrap());
    }

    let image = image::open(path).unwrap();
    let size = image.dimensions();

    let mut tiles = Vec::with_capacity((size.0 * size.1) as usize);
    tiles.extend(image.pixels().map(|(_x, _y, color)| {
        let cost = match color.to_rgb().channels() {
            [255, 0, 0] => 0.0,
            [0, 0, 255] => 0.0,
            [r, _, _] => *r as f32 / 255.0,
            _ => unreachable!(),
        };

        Tile::new(cost)
    }));

    Grid::new(size, tiles)
}

fn grid_dummy(width: u32, height: u32) -> Grid {
    let mut tiles = Vec::with_capacity((width * height) as usize);

    let max_cost = (width * height) as f32;

    for y in 0..height {
        for x in 0..width {
            let tile = Tile::new((x * y) as f32 / max_cost);
            tiles.push(tile);
        }
    }

    Grid::new((width, height), tiles)
}

fn display_grid_cli(grid: &Grid) {
    let max_cost = grid.max_cost();

    for y in (0..grid.height()).step_by(2) {
        for x in 0..grid.width() {
            let tile_top = grid.get_tile_at_coord(x, y);
            let tile_bottom = grid.get_maybe_tile_at_coord(x, y + 1);

            let cost_top_255 = (tile_top.cost / max_cost * 255.0).min(255.0) as u8;

            match tile_bottom {
                Some(tile_bottom) => {
                    let cost_bottom_255 = (tile_bottom.cost / max_cost * 255.0).min(255.0) as u8;
                    print!(
                        "\x1B[38;2;{};{};{};48;2;{};{};{}m▀",
                        cost_top_255,
                        cost_top_255,
                        cost_top_255,
                        cost_bottom_255,
                        cost_bottom_255,
                        cost_bottom_255,
                    );
                }
                None => {
                    print!(
                        "\x1B[0;38;2;{};{};{}m▀",
                        cost_top_255, cost_top_255, cost_top_255,
                    );
                }
            }
        }

        println!("\x1B[0m");
    }

    todo!()
}

fn display_grid_cli_big(grid: &Grid) {
    // let tiles = &grid.tiles;

    for y in 0..grid.height() {
        for x in 0..grid.width() {
            let tile = grid.get_tile_at_coord(x, y);
            let cost_255 = (tile.cost * 255.0) as u8;

            print!("\x1B[38;2;{};{};{}m██", cost_255, cost_255, cost_255);
        }

        println!();
    }

    print!("\x1B[0m");
    let _ = stdout().lock().flush();

    todo!()
}

struct Tile {
    cost: f32,
}

impl Tile {
    fn new(cost: f32) -> Self {
        Self { cost }
    }
}

struct Grid {
    size: (u32, u32),
    tiles: Vec<Tile>,
}

impl Grid {
    fn new(size: (u32, u32), tiles: Vec<Tile>) -> Self {
        // Make sure that all cells are filled.
        assert!(tiles.len() == (size.0 * size.1) as usize);

        // An empty grid is invalid.
        assert!(size.0 > 0 || size.1 > 0);

        Self { size, tiles }
    }

    fn width(&self) -> u32 {
        self.size.0
    }

    fn height(&self) -> u32 {
        self.size.1
    }

    pub fn get_tile_at_coord(&self, x: u32, y: u32) -> &Tile {
        if x > self.size.0 || y > self.size.1 {
            panic!("Coordinates out-of-bounds");
        }

        let index = self.index_2d_to_1d(x, y);
        &self.tiles[index]
    }

    pub fn get_maybe_tile_at_coord(&self, x: u32, y: u32) -> Option<&Tile> {
        if x >= self.size.0 || y >= self.size.1 {
            return None;
        }

        let index = self.index_2d_to_1d(x, y);
        Some(&self.tiles[index])
    }

    pub fn index_2d_to_1d(&self, x: u32, y: u32) -> usize {
        ((y * self.size.0) + x) as usize
    }

    pub fn max_cost(&self) -> f32 {
        self.tiles.iter().max_by(|a, b| a.cost.partial_cmp(&b.cost).unwrap()).unwrap().cost
    }
}
